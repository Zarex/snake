// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/SnakeGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeGameModeBase() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_ASnakeGameModeBase_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_ASnakeGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Snake();
	SNAKE_API UClass* Z_Construct_UClass_AFood_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKE_API UClass* Z_Construct_UClass_ABoost_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_ASnakeElementBase_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ASnakeGameModeBase::execBoostInit)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BoostInit();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ASnakeGameModeBase::execResetCreateBoostDalayTimer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetCreateBoostDalayTimer();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ASnakeGameModeBase::execResetBoostTimer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetBoostTimer();
		P_NATIVE_END;
	}
	void ASnakeGameModeBase::StaticRegisterNativesASnakeGameModeBase()
	{
		UClass* Class = ASnakeGameModeBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BoostInit", &ASnakeGameModeBase::execBoostInit },
			{ "ResetBoostTimer", &ASnakeGameModeBase::execResetBoostTimer },
			{ "ResetCreateBoostDalayTimer", &ASnakeGameModeBase::execResetCreateBoostDalayTimer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ASnakeGameModeBase_BoostInit_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASnakeGameModeBase_BoostInit_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASnakeGameModeBase_BoostInit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASnakeGameModeBase, nullptr, "BoostInit", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASnakeGameModeBase_BoostInit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASnakeGameModeBase_BoostInit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASnakeGameModeBase_BoostInit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASnakeGameModeBase_BoostInit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ASnakeGameModeBase_ResetBoostTimer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASnakeGameModeBase_ResetBoostTimer_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASnakeGameModeBase_ResetBoostTimer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASnakeGameModeBase, nullptr, "ResetBoostTimer", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASnakeGameModeBase_ResetBoostTimer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASnakeGameModeBase_ResetBoostTimer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASnakeGameModeBase_ResetBoostTimer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASnakeGameModeBase_ResetBoostTimer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ASnakeGameModeBase_ResetCreateBoostDalayTimer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASnakeGameModeBase_ResetCreateBoostDalayTimer_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ASnakeGameModeBase_ResetCreateBoostDalayTimer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASnakeGameModeBase, nullptr, "ResetCreateBoostDalayTimer", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASnakeGameModeBase_ResetCreateBoostDalayTimer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASnakeGameModeBase_ResetCreateBoostDalayTimer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASnakeGameModeBase_ResetCreateBoostDalayTimer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ASnakeGameModeBase_ResetCreateBoostDalayTimer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASnakeGameModeBase_NoRegister()
	{
		return ASnakeGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ASnakeGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnakeFood_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnakeFood;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnakeFoodClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SnakeFoodClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnakeBoost_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnakeBoost;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnakeBoostClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SnakeBoostClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SnakeMass_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnakeMass_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SnakeMass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElemSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElemSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MoveSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MoveSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OldMoveSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OldMoveSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpeedAfterBoost_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpeedAfterBoost;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CreateTimerDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CreateTimerDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoostLenght_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BoostLenght;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsQuit_MetaData[];
#endif
		static void NewProp_bIsQuit_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsQuit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnakeGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ASnakeGameModeBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ASnakeGameModeBase_BoostInit, "BoostInit" }, // 1573574903
		{ &Z_Construct_UFunction_ASnakeGameModeBase_ResetBoostTimer, "ResetBoostTimer" }, // 426724214
		{ &Z_Construct_UFunction_ASnakeGameModeBase_ResetCreateBoostDalayTimer, "ResetCreateBoostDalayTimer" }, // 3646588206
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "SnakeGameModeBase.h" },
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFood_MetaData[] = {
		{ "Category", "SnakeGameModeBase" },
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFood = { "SnakeFood", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, SnakeFood), Z_Construct_UClass_AFood_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFood_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFood_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFoodClass_MetaData[] = {
		{ "Category", "SnakeGameModeBase" },
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFoodClass = { "SnakeFoodClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, SnakeFoodClass), Z_Construct_UClass_AFood_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFoodClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFoodClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoost_MetaData[] = {
		{ "Category", "SnakeGameModeBase" },
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoost = { "SnakeBoost", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, SnakeBoost), Z_Construct_UClass_ABoost_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoost_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoost_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoostClass_MetaData[] = {
		{ "Category", "SnakeGameModeBase" },
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoostClass = { "SnakeBoostClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, SnakeBoostClass), Z_Construct_UClass_ABoost_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoostClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoostClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeMass_Inner = { "SnakeMass", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ASnakeElementBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeMass_MetaData[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeMass = { "SnakeMass", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, SnakeMass), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeMass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeMass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_ElemSize_MetaData[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_ElemSize = { "ElemSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, ElemSize), METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_ElemSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_ElemSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_MoveSpeed_MetaData[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_MoveSpeed = { "MoveSpeed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, MoveSpeed), METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_MoveSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_MoveSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_OldMoveSpeed_MetaData[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_OldMoveSpeed = { "OldMoveSpeed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, OldMoveSpeed), METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_OldMoveSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_OldMoveSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SpeedAfterBoost_MetaData[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SpeedAfterBoost = { "SpeedAfterBoost", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, SpeedAfterBoost), METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SpeedAfterBoost_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SpeedAfterBoost_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_CreateTimerDelay_MetaData[] = {
		{ "Category", "SnakeGameModeBase" },
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_CreateTimerDelay = { "CreateTimerDelay", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, CreateTimerDelay), METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_CreateTimerDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_CreateTimerDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_BoostLenght_MetaData[] = {
		{ "Category", "SnakeGameModeBase" },
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_BoostLenght = { "BoostLenght", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASnakeGameModeBase, BoostLenght), METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_BoostLenght_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_BoostLenght_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_bIsQuit_MetaData[] = {
		{ "ModuleRelativePath", "SnakeGameModeBase.h" },
	};
#endif
	void Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_bIsQuit_SetBit(void* Obj)
	{
		((ASnakeGameModeBase*)Obj)->bIsQuit = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_bIsQuit = { "bIsQuit", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ASnakeGameModeBase), &Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_bIsQuit_SetBit, METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_bIsQuit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_bIsQuit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASnakeGameModeBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFood,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeFoodClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoost,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeBoostClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeMass_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SnakeMass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_ElemSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_MoveSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_OldMoveSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_SpeedAfterBoost,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_CreateTimerDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_BoostLenght,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASnakeGameModeBase_Statics::NewProp_bIsQuit,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnakeGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnakeGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnakeGameModeBase_Statics::ClassParams = {
		&ASnakeGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ASnakeGameModeBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ASnakeGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnakeGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnakeGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnakeGameModeBase, 2389721446);
	template<> SNAKE_API UClass* StaticClass<ASnakeGameModeBase>()
	{
		return ASnakeGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnakeGameModeBase(Z_Construct_UClass_ASnakeGameModeBase, &ASnakeGameModeBase::StaticClass, TEXT("/Script/Snake"), TEXT("ASnakeGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnakeGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
