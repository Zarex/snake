// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_Boost_generated_h
#error "Boost.generated.h already included, missing '#pragma once' in Boost.h"
#endif
#define SNAKE_Boost_generated_h

#define Snake_Source_Snake_Boost_h_15_SPARSE_DATA
#define Snake_Source_Snake_Boost_h_15_RPC_WRAPPERS
#define Snake_Source_Snake_Boost_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_Boost_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABoost(); \
	friend struct Z_Construct_UClass_ABoost_Statics; \
public: \
	DECLARE_CLASS(ABoost, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(ABoost) \
	virtual UObject* _getUObject() const override { return const_cast<ABoost*>(this); }


#define Snake_Source_Snake_Boost_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABoost(); \
	friend struct Z_Construct_UClass_ABoost_Statics; \
public: \
	DECLARE_CLASS(ABoost, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(ABoost) \
	virtual UObject* _getUObject() const override { return const_cast<ABoost*>(this); }


#define Snake_Source_Snake_Boost_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABoost(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABoost) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABoost); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABoost); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABoost(ABoost&&); \
	NO_API ABoost(const ABoost&); \
public:


#define Snake_Source_Snake_Boost_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABoost(ABoost&&); \
	NO_API ABoost(const ABoost&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABoost); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABoost); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABoost)


#define Snake_Source_Snake_Boost_h_15_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_Boost_h_12_PROLOG
#define Snake_Source_Snake_Boost_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_Boost_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_Boost_h_15_SPARSE_DATA \
	Snake_Source_Snake_Boost_h_15_RPC_WRAPPERS \
	Snake_Source_Snake_Boost_h_15_INCLASS \
	Snake_Source_Snake_Boost_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_Boost_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_Boost_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_Boost_h_15_SPARSE_DATA \
	Snake_Source_Snake_Boost_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_Boost_h_15_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_Boost_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class ABoost>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_Boost_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
