// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/FoodElement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFoodElement() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_AFoodElement_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_AFoodElement();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake();
// End Cross Module References
	void AFoodElement::StaticRegisterNativesAFoodElement()
	{
	}
	UClass* Z_Construct_UClass_AFoodElement_NoRegister()
	{
		return AFoodElement::StaticClass();
	}
	struct Z_Construct_UClass_AFoodElement_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFoodElement_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodElement_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FoodElement.h" },
		{ "ModuleRelativePath", "FoodElement.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFoodElement_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFoodElement>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFoodElement_Statics::ClassParams = {
		&AFoodElement::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFoodElement_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodElement_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFoodElement()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFoodElement_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFoodElement, 4008484423);
	template<> SNAKE_API UClass* StaticClass<AFoodElement>()
	{
		return AFoodElement::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFoodElement(Z_Construct_UClass_AFoodElement, &AFoodElement::StaticClass, TEXT("/Script/Snake"), TEXT("AFoodElement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFoodElement);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
