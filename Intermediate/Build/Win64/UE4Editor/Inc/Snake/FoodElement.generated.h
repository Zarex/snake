// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_FoodElement_generated_h
#error "FoodElement.generated.h already included, missing '#pragma once' in FoodElement.h"
#endif
#define SNAKE_FoodElement_generated_h

#define Snake_Source_Snake_FoodElement_h_12_SPARSE_DATA
#define Snake_Source_Snake_FoodElement_h_12_RPC_WRAPPERS
#define Snake_Source_Snake_FoodElement_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_FoodElement_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFoodElement(); \
	friend struct Z_Construct_UClass_AFoodElement_Statics; \
public: \
	DECLARE_CLASS(AFoodElement, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AFoodElement)


#define Snake_Source_Snake_FoodElement_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFoodElement(); \
	friend struct Z_Construct_UClass_AFoodElement_Statics; \
public: \
	DECLARE_CLASS(AFoodElement, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AFoodElement)


#define Snake_Source_Snake_FoodElement_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFoodElement(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFoodElement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodElement); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodElement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodElement(AFoodElement&&); \
	NO_API AFoodElement(const AFoodElement&); \
public:


#define Snake_Source_Snake_FoodElement_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodElement(AFoodElement&&); \
	NO_API AFoodElement(const AFoodElement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodElement); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodElement); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFoodElement)


#define Snake_Source_Snake_FoodElement_h_12_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_FoodElement_h_9_PROLOG
#define Snake_Source_Snake_FoodElement_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_FoodElement_h_12_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_FoodElement_h_12_SPARSE_DATA \
	Snake_Source_Snake_FoodElement_h_12_RPC_WRAPPERS \
	Snake_Source_Snake_FoodElement_h_12_INCLASS \
	Snake_Source_Snake_FoodElement_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_FoodElement_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_FoodElement_h_12_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_FoodElement_h_12_SPARSE_DATA \
	Snake_Source_Snake_FoodElement_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_FoodElement_h_12_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_FoodElement_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class AFoodElement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_FoodElement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
