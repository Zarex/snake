// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameModeBase.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "Boost.h"
#include "Kismet/GameplayStatics.h"


ASnakeGameModeBase::ASnakeGameModeBase()
{

}

void ASnakeGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	BoostInit();

}

int ASnakeGameModeBase::RandCord(int min, int max)
{
	static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
	return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

void ASnakeGameModeBase::CreateSnakeFood()
{
	
	int tmpX = (RandCord(-9, 9))*ElemSize;
	int tmpY = (RandCord(-9, 9))*ElemSize;
	
	for (int i = SnakeMass.Num()-1; i >= 0; i--)
	{
		auto MassElem = SnakeMass[i];
		FVector MassLoc = MassElem->GetActorLocation();
		if (MassLoc == FVector(tmpX, tmpY, 0))
		{
			tmpX = (RandCord(-9, 9)) * ElemSize;
			tmpY = (RandCord(-9, 9)) * ElemSize;
		}

	}
	int x = tmpX;
	int y = tmpY;

	srand(static_cast<unsigned int>(time(0)));
	rand();
	
	
	SnakeFood = GetWorld()->SpawnActor<AFood>(SnakeFoodClass, FVector(x, y, 0), FRotator::ZeroRotator);
}

void ASnakeGameModeBase::CreateSnakeBoost()
{
	int tmpX = (RandCord(-9, 9)) * ElemSize;
	int tmpY = (RandCord(-9, 9)) * ElemSize;

	FVector Food = SnakeFood->GetActorLocation();
	if (Food == FVector(tmpX, tmpY, 0))
	{
		tmpX = (RandCord(-9, 9)) * ElemSize;
		tmpY = (RandCord(-9, 9)) * ElemSize;
	}

	for (int i = SnakeMass.Num() - 1; i >= 0; i--)
	{
		auto MassElem = SnakeMass[i];
		FVector MassLoc = MassElem->GetActorLocation();
		if (MassLoc == FVector(tmpX, tmpY, 0))
		{
			tmpX = (RandCord(-9, 9)) * ElemSize;
			tmpY = (RandCord(-9, 9)) * ElemSize;
		}

	}
	int x = tmpX;
	int y = tmpY;

	srand(static_cast<unsigned int>(time(0)));
	rand();


	SnakeBoost = GetWorld()->SpawnActor<ABoost>(SnakeBoostClass, FVector(x, y, 0), FRotator::ZeroRotator);
}

void ASnakeGameModeBase::BoostTimer()
{
	OldMoveSpeed = MoveSpeed;
	SpeedAfterBoost = 0.1;
	MoveSpeed = SpeedAfterBoost;
	GetWorldTimerManager().SetTimer(BoostDelayTimerHandle, this, &ASnakeGameModeBase::ResetBoostTimer , BoostLenght, false);
}

void ASnakeGameModeBase::ResetBoostTimer()
{

	SpeedAfterBoost = OldMoveSpeed;
	MoveSpeed = SpeedAfterBoost;
	GetWorldTimerManager().ClearTimer(BoostDelayTimerHandle);
	BoostInit();

}

void ASnakeGameModeBase::ResetCreateBoostDalayTimer()
{
	GetWorldTimerManager().ClearTimer(CreateBoostDalay);
	CreateSnakeBoost();
}

void ASnakeGameModeBase::BoostInit()
{
	GetWorldTimerManager().SetTimer(CreateBoostDalay, this, &ASnakeGameModeBase::ResetCreateBoostDalayTimer,CreateTimerDelay, false);
}

