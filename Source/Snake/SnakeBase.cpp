// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameModeBase.h"
#include <string>


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
 	ElementSize = 100.f;
 	LastMoveDirection = EMovementDirection::DOWN;	
	
	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	CanDestroy = false;
	ASnakeGameModeBase* GameMode = Cast<ASnakeGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		if (IsValid(GameMode))
		{
			GameMode->MoveSpeed = MovementSpeed;
		}
		
	
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	ASnakeGameModeBase* GameMode = Cast<ASnakeGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(GameMode))
	{
		MovementSpeed = GameMode->MoveSpeed;
	}
	if (CanDestroy)
	{
		SnakeElementsDelete();
	}
	
	SetActorTickInterval(MovementSpeed);
	
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{

		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		NewSnakeElem->HideElement(true);
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();

		}


	}
	ASnakeGameModeBase* GameMode = Cast<ASnakeGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(GameMode))
	{
		GameMode->SnakeMass = SnakeElements;
		GameMode->ElemSize = ElementSize;
	}

	
}


	
void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	CanMove = true;
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	
	}
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollison();
	

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		SnakeElements[i]->HideElement(false);
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	auto CurLoc = SnakeElements[0];
	FVector CurrLoc = CurLoc->GetActorLocation();
		if (CurrLoc.Y == -600 && LastMoveDirection==EMovementDirection::LEFT)
		{
			CurrLoc.Y += 1260;
			SnakeElements[0]->SetActorLocation(CurrLoc);
		}
		else if (CurrLoc.Y == 600 && LastMoveDirection == EMovementDirection::RIGHT)
		{
			CurrLoc.Y -= 1260;
			SnakeElements[0]->SetActorLocation(CurrLoc);
		}
		else if (CurrLoc.X == -600 && LastMoveDirection == EMovementDirection::DOWN)
		{
			CurrLoc.X += 1260;
			SnakeElements[0]->SetActorLocation(CurrLoc);
		}
		else if(CurrLoc.X == 600 && LastMoveDirection == EMovementDirection::UP)
		{
			CurrLoc.X -= 1260;
			SnakeElements[0]->SetActorLocation(CurrLoc);
		}
		


	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollison();
	SnakeElements[0]->HideElement(false);
	
	
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bisFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bisFirst);
		}
	}

}

void ASnakeBase::SnakeElementsDelete()
{
	for (int i = SnakeElements.Num() - 1; i >= 0; i--)
		{
			SnakeElements[i]->Destroy();
		}

}

