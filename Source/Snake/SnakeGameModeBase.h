// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameModeBase.generated.h"

class AFood;
class ASnakeBase;
class ASnakeElementBase;
class ABoost;



UCLASS()
class SNAKE_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:

	ASnakeGameModeBase();
			
	UPROPERTY(BlueprintReadWrite)
		AFood* SnakeFood;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> SnakeFoodClass;

	UPROPERTY(BlueprintReadWrite)
		ABoost* SnakeBoost;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABoost> SnakeBoostClass;

	
	int RandCord(int min, int max);

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeMass;
	
	UPROPERTY()
		float ElemSize;

	UPROPERTY()
		float MoveSpeed;

	UPROPERTY()
		float OldMoveSpeed;

	UPROPERTY()
		float SpeedAfterBoost;

	UPROPERTY(EditDefaultsOnly)
		float CreateTimerDelay;

	UPROPERTY(EditDefaultsOnly)
		float BoostLenght;

	UPROPERTY()
		bool bIsQuit=false;

	
public:

	void CreateSnakeFood();
	
	void CreateSnakeBoost();

	void BoostTimer();

	UFUNCTION()
		void ResetBoostTimer();

	UFUNCTION()
		void ResetCreateBoostDalayTimer();
	
	UFUNCTION()
		void BoostInit();

	FTimerHandle BoostDelayTimerHandle;

	FTimerHandle CreateBoostDalay;

protected:

	void BeginPlay() override;
};
