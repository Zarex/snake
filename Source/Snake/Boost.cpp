// Fill out your copyright notice in the Description page of Project Settings.


#include "Boost.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameModeBase.h"
#include "Countdown.h"



// Sets default values
ABoost::ABoost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	
}

// Called when the game starts or when spawned
void ABoost::BeginPlay()
{
	Super::BeginPlay();

	
}

// Called every frame
void ABoost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void ABoost::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeGameModeBase* GameMode = Cast<ASnakeGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		if (IsValid(GameMode))
		{
			GameMode->BoostTimer();
		}

		Countdown = GetWorld()->SpawnActor<ACountdown>(CountdownClass, FVector(300, 900, 100), FRotator(90,-180,0));

		ABoost::Destroy();
	}
}

